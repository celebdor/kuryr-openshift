============
Installation
============

At the command line::

    $ pip install kuryr-openshift

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv kuryr-openshift
    $ pip install kuryr-openshift
