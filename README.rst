===============================
kuryr-openshift
===============================

Kuryr handlers and drivers for OpenShift

This project is dedicated to providing kuryr-kubernetes handlers and drivers
for a better integration with OpenShift Origin.

* Free software: Apache license
* Source: http://gitlab.com/celebdor/kuryr-openshift
* Bugs: https://gitlab.com/celebdor/kuryr-openshift/issues

Features
--------

* TODO
